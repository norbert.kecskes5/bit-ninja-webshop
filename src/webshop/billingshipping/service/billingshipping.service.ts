import { MockedBillingShippings } from './../../../mocks/billingshipping.mocks';
import { BillingShippingModel } from './../model/billingshipping.model';
import { Injectable } from '@nestjs/common';

@Injectable()
export class BillingShippingService {
    
    private billingShippings: BillingShippingModel[] = MockedBillingShippings;
    
    public findAll(): BillingShippingModel[] {
        return this.billingShippings;
    }

    public findOne(id: number): BillingShippingModel {
        return this.billingShippings.find(billingShipping => billingShipping.id == id);
    }
}
