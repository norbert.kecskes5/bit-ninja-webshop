export class BillingShippingModel {
    id: number;
    type: Type;
    name: string;
    country: string;
    city: string;
    zip: number;
    address: string;
}

export enum Type {
    Billing,
    Shipping
}