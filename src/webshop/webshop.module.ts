import { OrderService } from './order/service/order.service';
import { BillingShippingService } from './billingshipping/service/billingshipping.service';
import { CustomerService } from './customer/service/customer.service';
import { ProductService } from './product/service/product.service';
import { ProductController } from './product/controller/product.controller';
import { Module } from '@nestjs/common';
import { CustomerController } from './customer/controller/customer.controller';
import { OrderController } from './order/controller/order.controller';

@Module({
    imports: [],
    controllers: [
        ProductController,
        CustomerController,
        OrderController
    ],
    providers: [
        ProductService,
        CustomerService,
        BillingShippingService,
        OrderService
    ],
})
export class WebshopModule {}
