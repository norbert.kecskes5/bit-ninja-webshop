import { HttpException, HttpStatus } from '@nestjs/common';

export class ProductDoesntExistException extends HttpException {
  constructor() {
    super('ProductDoesntExist', HttpStatus.NOT_FOUND);
  }
}
