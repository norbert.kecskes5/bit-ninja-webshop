import { HttpException, HttpStatus } from '@nestjs/common';

export class InsufficientQuantityException extends HttpException {
  constructor() {
    super('InsufficientQuantity', HttpStatus.UNPROCESSABLE_ENTITY);
  }
}
