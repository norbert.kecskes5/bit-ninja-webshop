import { HttpException, HttpStatus } from '@nestjs/common';

export class CustomerDoesntExistException extends HttpException {
  constructor() {
    super('CustomerDoesntExist', HttpStatus.NOT_FOUND);
  }
}
