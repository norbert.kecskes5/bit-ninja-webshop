import { CreateOrderDto } from './../dto/create-order.dto';
import { OrderModel, OrderStatus } from './../model/product.model';
import { OrderService } from './../service/order.service';
import { Controller, Get, Param, Post, Body } from '@nestjs/common';

@Controller('orders')
export class OrderController {

    constructor(private orderService: OrderService){}

    @Get()
    findAll(): OrderModel[] {
        return this.orderService.findAll();
    }

    @Get(':orderStatus')
    findAllByOrderStatus(@Param('orderStatus') orderStatus:OrderStatus): OrderModel[] {
        return this.orderService.findAllByOrderStatus(orderStatus);
    }

    @Post()
    createOrder(@Body() createOrderDto: CreateOrderDto): OrderModel {
        return this.orderService.createOrder(createOrderDto);
    }
}
