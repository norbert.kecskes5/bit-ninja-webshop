export class OrderModel {
    id: number;
    productId: number;
    customerId: number;
    quantity: number;
    priceTotal: number;
    orderDate: Date;
    lastModifyDate: Date;
    paymentType: PaymentType;
    orderStatus: OrderStatus;
}

export enum PaymentType {
    Cash = "Cash",
    Transfer = "Transfer"
}

export enum OrderStatus {
    Processing = "Processing",
    Shipping = "Shipping",
    Completed = "Completed"
}