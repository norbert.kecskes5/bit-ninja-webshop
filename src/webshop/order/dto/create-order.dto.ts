import { PaymentType } from './../model/product.model';

export class CreateOrderDto {
    productId: number;
    customerId: number;
    quantity: number;
    paymentType: PaymentType;
}