import { InsufficientQuantityException } from './../exception/insufficientquantity.exception';
import { CustomerModel } from './../../customer/model/customer.model';
import { CustomerService } from './../../customer/service/customer.service';
import { ProductModel } from './../../product/model/product.model';
import { ProductService } from './../../product/service/product.service';
import { CreateOrderDto } from './../dto/create-order.dto';
import { MockedOrders } from './../../../mocks/order.mocks';
import { OrderModel, OrderStatus } from './../model/product.model';
import { Injectable } from '@nestjs/common';
import { ProductDoesntExistException } from '../exception/productdoesntexist.exception';
import { CustomerDoesntExistException } from '../exception/customerdoesntexist.exception';

@Injectable()
export class OrderService {

    constructor(
        private productService: ProductService,
        private customerService: CustomerService
    ) {}

    private orders: OrderModel[] = MockedOrders;
    
    public findAll(): OrderModel[] {
        return this.orders;
    }

    public findOne(id: number): OrderModel {
        return this.orders.find(order => order.id == id);
    }

    public findAllByOrderStatus(orderStatus: OrderStatus): OrderModel[] {
        return this.orders.filter(order => order.orderStatus == orderStatus);
    }

    public createOrder(createOrderDto: CreateOrderDto): OrderModel {
        const newOrder: OrderModel = this.createAndValidateNewOrder(createOrderDto);
        this.orders.push(newOrder);

        return newOrder;
    }

    private createAndValidateNewOrder(createOrderDto: CreateOrderDto): OrderModel {
        const orderedProduct: ProductModel = this.productService.findOne(createOrderDto.productId);
        const customer: CustomerModel = this.customerService.findOne(createOrderDto.customerId);

        this.validate(orderedProduct, customer, createOrderDto.quantity);

        this.productService.updateQuantity(orderedProduct.itemNumber, createOrderDto.quantity);
        
        const newOrder: OrderModel = {
            id: this.getMaxId() + 1,
            productId: createOrderDto.productId,
            customerId: createOrderDto.customerId,
            quantity: createOrderDto.quantity,
            priceTotal: createOrderDto.quantity * orderedProduct.pricePerItem,
            orderDate: new Date(),
            lastModifyDate: new Date(),
            paymentType: createOrderDto.paymentType,
            orderStatus: OrderStatus.Processing
        }

        return newOrder;
    }

    private validate(orderedProduct: ProductModel, customer: CustomerModel, quantity: number) {
        if(orderedProduct == null) {
            throw new ProductDoesntExistException();
        }

        if(customer == null) {
            throw new CustomerDoesntExistException();
        }

        if(quantity > orderedProduct.quantity) {
            throw new InsufficientQuantityException();
        }
    }

    private getMaxId(): number {
        console.log(this.orders);
        return this.orders.sort((a, b) => {
            return a.id - b.id;
        })[this.orders.length - 1].id;
    }
}
