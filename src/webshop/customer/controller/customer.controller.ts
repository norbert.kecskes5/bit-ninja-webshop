import { CustomerResponseModel } from './../model/response/customer.responsemodel';
import { CustomerModel } from './../model/customer.model';
import { CustomerService } from './../service/customer.service';
import { Controller, Get } from '@nestjs/common';

@Controller('customers')
export class CustomerController {
    constructor(private customerService: CustomerService){}

    @Get()
    findAll(): CustomerModel[] {
        return this.customerService.findAll();
    }

    @Get('detailed')
    listWithDetails(): CustomerResponseModel[] {
        return this.customerService.listWithDetails();
    }
}
