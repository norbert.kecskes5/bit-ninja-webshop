export class CustomerModel {
    id: number;
    name: string;
    email: string;
    birthDate: Date;
    shippingAddressId: number;
    billingAddressId: number;
}