import { BillingShippingModel } from './../../../billingshipping/model/billingshipping.model';

export class CustomerResponseModel {
    id: number;
    name: string;
    email: string;
    birthDate: Date;
    shippingAddress: BillingShippingModel;
    billingAddress: BillingShippingModel;
}