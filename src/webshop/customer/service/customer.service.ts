import { BillingShippingService } from './../../billingshipping/service/billingshipping.service';
import { CustomerResponseModel } from './../model/response/customer.responsemodel';
import { CustomerModel } from './../model/customer.model';
import { Injectable } from '@nestjs/common';
import { MockedCustomers } from 'src/mocks/customer.mocks';

@Injectable()
export class CustomerService {

    constructor(private billingShippingService: BillingShippingService) {}

    private customers: CustomerModel[] = MockedCustomers;
    
    public findAll(): CustomerModel[] {
        return this.customers;
    }

    public findOne(id: number): CustomerModel {
        return this.customers.find(customer => customer.id == id);
    }

    public listWithDetails(): CustomerResponseModel[] {
        return this.customers.map(customer => this.createCustomerReponseModel(customer));
    }

    private createCustomerReponseModel(customer: CustomerModel): CustomerResponseModel {
        const customerResponse: CustomerResponseModel = {
            id: customer.id,
            name: customer.name,
            email: customer.email,
            birthDate: customer.birthDate,
            shippingAddress: this.billingShippingService.findOne(customer.shippingAddressId),
            billingAddress: this.billingShippingService.findOne(customer.billingAddressId)
        }

        return customerResponse;
    }
}
