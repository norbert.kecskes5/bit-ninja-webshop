import { HttpException, HttpStatus } from '@nestjs/common';

export class ItemNumberExistsException extends HttpException {
  constructor() {
    super('ItemNumberExists', HttpStatus.CONFLICT);
  }
}
