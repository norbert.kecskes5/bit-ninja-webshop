export class ProductModel {
    name: string;
    itemNumber: number;
    description: string;
    manufacturer: string;
    pricePerItem: number;
    quantity: number;
}