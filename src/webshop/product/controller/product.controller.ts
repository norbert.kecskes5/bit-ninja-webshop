import { CreateProductDto } from './../dto/create-product.dto';
import { ProductService } from '../service/product.service';
import { Controller, Get, Post, Body } from '@nestjs/common';
import { ProductModel } from '../model/product.model';

@Controller('products')
export class ProductController {

    constructor(private productService: ProductService){}

    @Get()
    findAll(): ProductModel[] {
        return this.productService.findAll();
    }

    @Get('outofstock')
    findAllOutOfStock(): ProductModel[] {
        return this.productService.findAllOutOfStock();
    }

    @Post()
    addProduct(@Body() createProductDto: CreateProductDto): ProductModel {
        return this.productService.addProduct(createProductDto);
    }
}
