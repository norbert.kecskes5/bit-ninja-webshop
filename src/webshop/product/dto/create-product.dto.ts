export class CreateProductDto {
    readonly name: string;
    readonly itemNumber: number;
    readonly description: string;
    readonly manufacturer: string;
    readonly pricePerItem: number;
    readonly quantity: number;
}