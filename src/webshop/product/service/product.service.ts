import { ProductModel } from './../model/product.model';
import { ItemNumberExistsException } from './../exception/itemnumberexists.exception';
import { MockedProducts } from './../../../mocks/product.mocks';
import { Injectable } from '@nestjs/common';

@Injectable()
export class ProductService {

    private products: ProductModel[] = MockedProducts;
    
    public findAll(): ProductModel[] {
        return this.products;
    }

    public findAllOutOfStock(): ProductModel[] {
        return this.products.filter(product => product.quantity == 0);
    }

    public findOne(id: number): ProductModel {
        return this.products.find(product => product.itemNumber == id);
    }

    public addProduct(product: ProductModel): ProductModel {
        this.validate(product);
        
        this.products.push(product);
        return product;
    }

    public updateQuantity(productId: number, quantity: number) {
        const updatedProduct: ProductModel = this.findOne(productId);
        const index = this.products.indexOf(updatedProduct);
    
        updatedProduct.quantity = updatedProduct.quantity - quantity;

        this.products[index] = updatedProduct;
    }

    private validate(product: ProductModel) {
        this.products.forEach(currentProduct => {
            if(currentProduct.itemNumber == product.itemNumber) {
                throw new ItemNumberExistsException();
            }
        })
    }
}
