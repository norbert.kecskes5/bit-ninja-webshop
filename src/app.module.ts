import { OrderController } from './webshop/order/controller/order.controller';
import { WebshopModule } from './webshop/webshop.module';
import { Module } from '@nestjs/common';

@Module({
  imports: [
        WebshopModule
      ],
  controllers: [],
  providers: [],
})
export class AppModule {}
