import { BillingShippingModel, Type } from './../webshop/billingshipping/model/billingshipping.model';

export const MockedBillingShippings: BillingShippingModel[] = [
    {
        id: 1,
        type: Type.Billing,
        name: "Kiss Ernő",
        country: "Magyarország",
        city: "Miskolc",
        zip: 3525,
        address: "Alma utca 2."
    },
    {
        id: 2,
        type: Type.Shipping,
        name: "Kiss Ernő",
        country: "Magyarország",
        city: "Miskolc",
        zip: 3525,
        address: "Alma utca 2."
    },
    {
        id: 3,
        type: Type.Shipping,
        name: "Nagy Éva",
        country: "Magyarország",
        city: "Mezőkövesd",
        zip: 3400,
        address: "Táncsics utca 27."
    },
    {
        id: 4,
        type: Type.Shipping,
        name: "Molnár Ádám",
        country: "Magyarország",
        city: "Miskolc",
        zip: 3525,
        address: "Erzsébet utca 17. 2. em. 6. a."
    },
    {
        id: 5,
        type: Type.Shipping,
        name: "Horváth Elemér",
        country: "Magyarország",
        city: "Miskolc",
        zip: 3531,
        address: "Petőfi utca 22."
    },
    {
        id: 6,
        type: Type.Shipping,
        name: "Tóth Máté",
        country: "Magyarország",
        city: "Mezőkövesd",
        zip: 3531,
        address: "Eper utca 117."
    }
]