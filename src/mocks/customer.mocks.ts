import { CustomerModel } from './../webshop/customer/model/customer.model';

export const MockedCustomers: CustomerModel[] = [
    {
        id: 1,
        name: "Kiss Ernő",
        email: "erno.kiss22@gmail.com",
        birthDate: new Date("1995-01-16"),
        shippingAddressId: 2,
        billingAddressId: 1
    },
    {
        id: 2,
        name: "Nagy Éva",
        email: "nagy.eva@gmail.com",
        birthDate: new Date("1973-03-21"),
        shippingAddressId: 3,
        billingAddressId: null
    },
    {
        id: 3,
        name: "Molnár Ádám",
        email: "molnar.adam@gmail.com",
        birthDate: new Date("2000-12-11"),
        shippingAddressId: 4,
        billingAddressId: null
    },
    {
        id: 4,
        name: "Horváth Elemér",
        email: "laposelemer92@gmail.com",
        birthDate: new Date("1992-01-05"),
        shippingAddressId: 5,
        billingAddressId: null
    },
    {
        id: 5,
        name: "Tóth Máté",
        email: "toth1963@gmail.com",
        birthDate: new Date("1963-04-17"),
        shippingAddressId: 6,
        billingAddressId: 1
    }
];
