import { OrderModel, PaymentType, OrderStatus } from './../webshop/order/model/product.model';

export const MockedOrders: OrderModel[] = [
    {
        id: 1,
        productId: 1,
        customerId: 1,
        quantity: 1,
        priceTotal: 100000,
        orderDate: new Date(),
        lastModifyDate: new Date(),
        paymentType: PaymentType.Cash,
        orderStatus: OrderStatus.Processing
    },
    {
        id: 2,
        productId: 2,
        customerId: 2,
        quantity: 2,
        priceTotal: 300000,
        orderDate: new Date(),
        lastModifyDate: new Date(),
        paymentType: PaymentType.Cash,
        orderStatus: OrderStatus.Completed
    },
    {
        id: 3,
        productId: 3,
        customerId: 3,
        quantity: 1,
        priceTotal: 350000,
        orderDate: new Date(),
        lastModifyDate: new Date(),
        paymentType: PaymentType.Transfer,
        orderStatus: OrderStatus.Shipping
    },
    {
        id: 4,
        productId: 4,
        customerId: 4,
        quantity: 1,
        priceTotal: 20000,
        orderDate: new Date(),
        lastModifyDate: new Date(),
        paymentType: PaymentType.Cash,
        orderStatus: OrderStatus.Processing
    },
    {
        id: 5,
        productId: 5,
        customerId: 5,
        quantity: 1,
        priceTotal: 5000,
        orderDate: new Date(),
        lastModifyDate: new Date(),
        paymentType: PaymentType.Transfer,
        orderStatus: OrderStatus.Processing
    },
]