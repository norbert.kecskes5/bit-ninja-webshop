import { ProductModel } from '../webshop/product/model/product.model';

export const MockedProducts: ProductModel[] = [
    {
        name: "PS4",
        itemNumber: 1,
        description: "lorem ipsum",
        manufacturer: "SONY",
        pricePerItem: 100000,
        quantity: 10
    },
    {
        name: "SmartTV",
        itemNumber: 2,
        description: "lorem ipsum",
        manufacturer: "SAMSUNG",
        pricePerItem: 150000,
        quantity: 2
    },
    {
        name: "iPhone 11",
        itemNumber: 3,
        description: "lorem ipsum",
        manufacturer: "Apple",
        pricePerItem: 350000,
        quantity: 5
    },
    {
        name: "Gaming Mouse",
        itemNumber: 4,
        description: "lorem ipsum",
        manufacturer: "Razer",
        pricePerItem: 20000,
        quantity: 50
    },
    {
        name: "Gaming Mouse",
        itemNumber: 5,
        description: "lorem ipsum",
        manufacturer: "TRUST",
        pricePerItem: 5000,
        quantity: 0
    }
];
